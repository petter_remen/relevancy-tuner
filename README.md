# Dropwizard Techtalk

This is the source code for the tech talk on the 22nd of October 2014 @ Findwise GBG. The talk is available at http://www.youtube.com/watch?v=5fX06fqBAUc

## Compile, run migrations and run server
```
$ mvn clean package
$ java -jar target/relevancy-tuner-1.0-SNAPSHOT.jar db migrate config/development.yml
$ java -jar target/relevancy-tuner-1.0-SNAPSHOT.jar server config/development.yml
```

## Storing a query
```
$ curl -i -XPOST -H "Content-Type: application/json" localhost:8080/queries -d @- << EOF                                                                             git:master* 
{
 "ident" : "default",
 "searcher" : "people",
 "query": "andreas"
}
EOF

HTTP/1.1 201 Created
Date: Wed, 22 Oct 2014 12:28:17 GMT
Location: http://localhost:8080/queries/1
Content-Type: application/json
Content-Length: 0
```

## Getting a stored query by ID
```
$ curl localhost:8080/queries/1
{"ident":"default","searcher":"people","query":"andreas"}%
```

## Getting results from jellyfish
```
$ curl localhost:8080/queries/1/results
[{"id":"andreas","phone":"+46 31 772 10 70", ...  # Not pretty printed :(
```