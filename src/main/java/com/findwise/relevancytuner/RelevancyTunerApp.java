package com.findwise.relevancytuner;

import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.skife.jdbi.v2.DBI;

import com.findwise.relevancytuner.dao.QueryDao;
import com.findwise.relevancytuner.jellyfish.JellyfishSearcher;
import com.findwise.relevancytuner.resources.QueryResource;

public class RelevancyTunerApp extends Application<RelevancyTunerConfig> {

    public static void main(String[] args) throws Exception {
        new RelevancyTunerApp().run(args);
    }

    @Override
    public void initialize(Bootstrap<RelevancyTunerConfig> bootstrap) {
        bootstrap.addBundle(new MigrationsBundle<RelevancyTunerConfig>() {
            @Override
            public DataSourceFactory getDataSourceFactory(RelevancyTunerConfig configuration) {
                return configuration.getDataSourceFactory();
            }
        });
    }

    @Override
    public void run(RelevancyTunerConfig configuration, Environment environment) throws Exception {
        DBI jdbi = new DBIFactory().build(environment, configuration.getDataSourceFactory(), "h2");
        QueryDao queryDao = jdbi.onDemand(QueryDao.class);

        JellyfishSearcher jellyfishSearcher = configuration.getJellyfishSearcherFactory().build();
        environment.jersey().register(new QueryResource(queryDao, jellyfishSearcher));
    }
}
