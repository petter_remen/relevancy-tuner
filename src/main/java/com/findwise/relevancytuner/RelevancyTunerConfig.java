package com.findwise.relevancytuner;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import com.findwise.relevancytuner.jellyfish.JellyfishSearcherFactory;

public class RelevancyTunerConfig extends Configuration {
    @JsonProperty
    @Valid
    @NotNull
    private DataSourceFactory database = new DataSourceFactory();

    public DataSourceFactory getDataSourceFactory() {
        return database;
    }

    @JsonProperty
    @Valid
    @NotNull
    private JellyfishSearcherFactory jellyfish = new JellyfishSearcherFactory();

    public JellyfishSearcherFactory getJellyfishSearcherFactory() {
        return jellyfish;
    }
}
