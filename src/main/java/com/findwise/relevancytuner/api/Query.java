package com.findwise.relevancytuner.api;

import org.hibernate.validator.constraints.NotEmpty;

public class Query {
    @NotEmpty
    private String ident;

    @NotEmpty
    private String searcher;

    @NotEmpty
    private String query;

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public String getSearcher() {
        return searcher;
    }

    public void setSearcher(String searcher) {
        this.searcher = searcher;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
