package com.findwise.relevancytuner.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import com.findwise.relevancytuner.api.Query;

public interface QueryDao {
    @SqlUpdate("INSERT INTO queries (ident, searcher, query) VALUES (:ident, :searcher, :query)")
    @GetGeneratedKeys
    int insertQuery(@BindBean Query query);

    @RegisterMapper(QueryMapper.class)
    @SqlQuery("SELECT * FROM queries WHERE id = :queryId")
    Query findQueryById(@Bind("queryId") int queryId);
}
