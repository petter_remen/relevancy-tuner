package com.findwise.relevancytuner.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.findwise.relevancytuner.api.Query;

public class QueryMapper implements ResultSetMapper<Query> {

    @Override
    public Query map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        Query query = new Query();
        query.setIdent(r.getString("ident"));
        query.setSearcher(r.getString("searcher"));
        query.setQuery(r.getString("query"));
        return query;
    }
}