package com.findwise.relevancytuner.jellyfish;

import javax.xml.bind.JAXBException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.findwise.jellyfish.domain.SearchException;
import com.findwise.jellyfish.domain.SearchResult;
import com.findwise.jellyfish.domain.doc.Document;
import com.findwise.jellyfish.domain.query.Query;
import com.findwise.jellyfish.domain.query.SearchParameter;
import com.findwise.jellyfish.jf4j.client.JaxbRestSearcher;
import com.findwise.jellyfish.jf4j.querycodec.UrlQueryEncoder;

public class JellyfishSearcher {
    private final String baseUrl;

    public JellyfishSearcher(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public List<Document> search(String identId, String searcherId, String queryString) {
        JaxbRestSearcher searcher;
        try {
            searcher = new JaxbRestSearcher();
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
        searcher.setQueryEncoder(new UrlQueryEncoder());
        searcher.setBaseUrl(baseUrl);
        searcher.setIdent(identId);
        searcher.setSearcherId(searcherId);
        Query query = new Query(
            Arrays.asList(new SearchParameter("q", Arrays.asList(queryString)))
        );

        SearchResult searchResult;
        try {
            searchResult = searcher.search(query);
        } catch (SearchException e) {
            throw new RuntimeException(e);
        }
        return searchResult.getDocuments();
    }
}
