package com.findwise.relevancytuner.jellyfish;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;

public class JellyfishSearcherFactory {
    @NotEmpty
    @URL
    @JsonProperty
    private String url;

    public JellyfishSearcher build() {
        return new JellyfishSearcher(url);
    }
}
