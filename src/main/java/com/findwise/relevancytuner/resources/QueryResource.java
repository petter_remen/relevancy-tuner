package com.findwise.relevancytuner.resources;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.findwise.jellyfish.domain.doc.Document;
import com.findwise.jellyfish.domain.doc.Field;
import com.findwise.relevancytuner.api.Query;
import com.findwise.relevancytuner.api.QueryResult;
import com.findwise.relevancytuner.dao.QueryDao;
import com.findwise.relevancytuner.jellyfish.JellyfishSearcher;

@Path("/queries")
@Produces("application/json")
public class QueryResource {

    private final QueryDao queryDao;
    private final JellyfishSearcher jellyfishSearcher;

    @Context
    UriInfo uriInfo;

    public QueryResource(QueryDao queryDao, JellyfishSearcher jellyfishSearcher) {
        this.queryDao = queryDao;
        this.jellyfishSearcher = jellyfishSearcher;
    }

    @POST
    public Response createQuery(@Valid Query query) {
        Integer id = queryDao.insertQuery(query);
        URI location = uriInfo.getAbsolutePathBuilder().path(id.toString()).build();
        return Response.created(location).build();    }

    @GET
    @Path("/{queryId}")
    public Query getQuery(@PathParam("queryId") int queryId) {
        return queryDao.findQueryById(queryId);
    }

    @GET
    @Path("/{queryId}/results")
    public List<QueryResult> getResults(@PathParam("queryId") int queryId) {
        Query query = queryDao.findQueryById(queryId);
        if(query == null) {
            throw new RuntimeException();
        }
        List<QueryResult> queryResults = new ArrayList<QueryResult>();
        for (Document document : jellyfishSearcher.search(query.getIdent(), query.getSearcher(), query.getQuery())) {
            QueryResult queryResult = documentToQueryResult(document, queryId);
            queryResults.add(queryResult);
        }
        return queryResults;
    }

    private QueryResult documentToQueryResult(Document document, int queryId) {
        QueryResult queryResult = new QueryResult();
        Map<String, Object> fields = new HashMap<String, Object>();
        for (Map.Entry<String, Field<?>> entry : document.getFields().entrySet()) {
            fields.put(entry.getKey(), entry.getValue().getValue());
        }
        queryResult.setFields(fields);
        return queryResult;
    }
}
